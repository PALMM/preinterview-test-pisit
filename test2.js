function main(n, i, j){
    let result = []
    for(let a = 1; a <= n; a++){
        if(a % i == 0 && a % j == 0){
            result.push(a + " Ping Pong")
        }else if(a % i == 0){
            result.push(a + " Ping ")
        }else if(a % j == 0){
            result.push(a + " Pong ")
        }
    }
    return result
}

const n = 15
const i = 4
const j = 10
console.log(main(n, i, j))