function main(x, y, m){

    const dateX = new Date(x.substring(0, 2)-x.substring(3, 5)-x.substring(6, 10))
    const dateY = new Date(y.substring(0, 2)-y.substring(3, 5)-y.substring(6, 10))
    const dateM = new Date(m.substring(0, 2)-m.substring(3, 5)-m.substring(6, 10))

    if(dateM > dateX && dateM < dateY){
        const date = Number(m.substring(0, 2)) - Number(x.substring(0, 2))
        const dates = date + " Days"
        return [true, dates]
    }else{
        const date = Number(m.substring(0, 2)) - Number(x.substring(0, 2))
        const dates = date + " Days"
        return [false,dates]
    }
}

const x = "09/09/2023"
const y = "12/09/2023"
const m = "10/09/2023"
console.log(main(x, y, m))