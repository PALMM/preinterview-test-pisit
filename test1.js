function main(n){
    let result = []
    for(let i=1; i<=n; i++){
        if(i % 3 == 0 && i % 5 == 0){
            result.push(i + " Ping Pong")
        }else if(i % 5 == 0){
            result.push(i + " Pong")
        }else if(i % 3 == 0 ){
            result.push(i + " Ping")
        }
    }
    return result
}

const n = 15
console.log(main(n))