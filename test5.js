function main(x) {
  const history = [];
  let nhistory = [];
  const resultHistory = [];

  for (let i = 0; i < x.length; i++) {
    if (x[i].includes("input")) {
      if (history.length == 0) {
        history.push(x[i].substr(6));
      } else {
        nhistory = [];
        history.push(x[i].substr(6));
      }
    } else if (x[i].includes("prev")) {
      if (history.length <= 1) {
        resultHistory.push("No website to previous");
      } else {
        nhistory.push(history.pop());
      }
    } else if (x[i].includes("next")) {
      if (nhistory.length == 0) {
        resultHistory.push("No website to go");
      } else {
        history.push(nhistory.shift());
      }
    } else if (x[i].includes("current")) {
      resultHistory.push("Now you on " + history[history.length - 1]);
    } else if (x[i].includes("all")) {
      resultHistory.push(history);
    }
  }
  return resultHistory;
}

const websiteHistory = [
  "prev",
  "input www.geerang.com",
  "input www.google.com",
  "next",
  "current",
  "prev",
  "next",
  "current",
  "prev",
  "input www.youtube.com",
  "next",
  "current",
  "all",
];

console.log(main(websiteHistory));
