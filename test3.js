function main(a, x){
    let result = []
    let index = []
    for(let i = 0; i < a.length; i++){
        if(a[i].toLowerCase().includes(x)){
            result.push(a[i])
            index.push(a[i].toLowerCase().indexOf(x))
        }
    }

    if (result.length == 0) {
        return "No results found";
    }
    return [result, index]
}

// const a = ['Cat', 'Bat', 'Dog', 'Bird']
// const a = ['Cat', 'Dog', 'Ant', 'Bird']
const a = ['Cat', 'Dog', 'Bird']
const x = 'd'
console.log(main(a, x))